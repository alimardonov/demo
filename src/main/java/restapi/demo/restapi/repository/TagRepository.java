package restapi.demo.restapi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import restapi.demo.restapi.entity.Tag;

@Repository
public interface TagRepository extends BaseRepository<Tag> {
    Page<Tag> findAllByTitleContainsAndDeletedFalse(String search, Pageable pageable);
}
