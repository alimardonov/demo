package restapi.demo.restapi.repository;

import org.springframework.stereotype.Repository;
import restapi.demo.restapi.entity.User;

import java.util.Optional;

@Repository
public interface UserRepository extends BaseRepository<User> {
    Optional<User> findByEmail(String email);
    boolean existsByEmail(String email);
}
