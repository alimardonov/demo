package restapi.demo.restapi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import restapi.demo.restapi.entity.Question;
import restapi.demo.restapi.entity.Tag;

@Repository
public interface QuestionRepository extends BaseRepository<Question> {
    Page<Question> findAllByCategory_IdAndDeletedFalse(Long id, Pageable pageable);
    Question findByTags(Tag tag);
}
