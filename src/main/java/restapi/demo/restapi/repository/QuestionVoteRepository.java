package restapi.demo.restapi.repository;

import org.springframework.stereotype.Repository;
import restapi.demo.restapi.dto.VoteType;
import restapi.demo.restapi.entity.Question;
import restapi.demo.restapi.entity.QuestionVote;
import restapi.demo.restapi.entity.User;

import java.util.Optional;

@Repository
public interface QuestionVoteRepository extends BaseRepository<QuestionVote> {

    Optional<QuestionVote> findByQuestion_IdAndCreatedByAndDeletedFalse(Long questionId, User user);

    long countByQuestionAndTypeAndDeletedFalse(Question question, VoteType type);
}
