package restapi.demo.restapi.repository;

import restapi.demo.restapi.entity.Answer;
import restapi.demo.restapi.entity.Question;

import java.util.List;

public interface AnswerRepository extends BaseRepository<Answer> {
    List<Answer> findAllByQuestionAndDeletedFalse(Question question);
}
