package restapi.demo.restapi.repository;

import org.springframework.stereotype.Repository;
import restapi.demo.restapi.entity.Category;

import java.util.List;

@Repository
public interface CategoryRepository extends BaseRepository<Category> {
    List<Category> findAllByParent_IdAndDeletedFalse(Long parentId);
}
