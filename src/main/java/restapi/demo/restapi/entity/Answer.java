package restapi.demo.restapi.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Setter
@Getter
public class Answer extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Question question;

    @Column(columnDefinition = "text")
    private String content;

    @Column(nullable = false)
    private Long voteCount = 0L;
}
