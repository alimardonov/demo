package restapi.demo.restapi.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;
import restapi.demo.restapi.dto.UserRole;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity(name = "my_user")
@Setter
@Getter
public class User extends BaseEntity {

    @Column(unique = true)
    private String email;

    private String password;

    @Column(nullable = false)
    @ColumnDefault("false")
    private boolean confirmed = false;

    private String fullName;

    @Enumerated(EnumType.STRING)
    private UserRole role = UserRole.USER;
}
