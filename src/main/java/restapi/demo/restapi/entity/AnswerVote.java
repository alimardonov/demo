package restapi.demo.restapi.entity;

import lombok.Getter;
import lombok.Setter;
import restapi.demo.restapi.dto.VoteType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

@Entity
@Setter
@Getter
public class AnswerVote extends BaseEntity {

    @ManyToOne
    private Answer answer;

    @Enumerated(EnumType.STRING)
    @Size(max = 10)
    private VoteType type;
}
