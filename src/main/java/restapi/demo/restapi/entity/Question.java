package restapi.demo.restapi.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Question extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Category category;

    private String title;

    @Column(columnDefinition = "text")
    private String description;

    @Column(nullable = false)
    private Long viewCount = 0L;

    @Column(nullable = false)
    private Long voteCount = 0L;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(inverseJoinColumns = {@JoinColumn(name = "tag_id")})
    private Set<Tag> tags;

    @Transient
    public Question incrementViewCount() {
        this.viewCount ++;
        return this;
    }
}