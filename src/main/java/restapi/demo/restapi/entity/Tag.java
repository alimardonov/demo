package restapi.demo.restapi.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Setter
@Getter
public class Tag extends BaseEntity {

    @Column(unique = true)
    private String title;
}
