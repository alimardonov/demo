package restapi.demo.restapi.entity;

import lombok.Getter;
import lombok.Setter;
import restapi.demo.restapi.dto.VoteType;

import javax.persistence.*;

@Entity
@Setter
@Getter
public class QuestionVote extends BaseEntity {

    @ManyToOne
    private Question question;

    @Enumerated(EnumType.STRING)
    private VoteType type;
}
