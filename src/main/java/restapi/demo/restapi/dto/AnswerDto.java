package restapi.demo.restapi.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import restapi.demo.restapi.entity.Answer;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AnswerDto {

    private Long id;

    private String content;

    private Long voteCount;

    private Long time;

    private UserDto author;

    public static AnswerDto fromAnswer(Answer answer){
        AnswerDto dto = new AnswerDto();
        dto.setId(answer.getId());
        dto.setContent(answer.getContent());
        dto.setVoteCount(answer.getVoteCount());
        dto.setTime(answer.getCreatedDate().getTime());
        dto.setAuthor(UserDto.fromUser(answer.getCreatedBy()));
        return dto;
    }
}
