package restapi.demo.restapi.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import restapi.demo.restapi.entity.Answer;
import restapi.demo.restapi.entity.Question;
import restapi.demo.restapi.entity.QuestionVote;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class QuestionDto extends QuestionListItemDto {

    private QuestionVoteDto vote;

    private List<AnswerDto> answers;

    public static class Builder {

        private Question question;

        private List<Answer> answers;

        private QuestionVote vote;

        public Builder(Question question) {
        }

        public Builder vote(QuestionVote vote) {
            this.vote = vote;
            return this;
        }

        public Builder answer(List<Answer> answers) {
            this.answers = answers;
            return this;
        }

        public QuestionDto builder() {
            QuestionDto dto = new QuestionDto();
            dto.setId(question.getId());
            dto.setTitle(question.getTitle());
            dto.setDescription(question.getDescription());
            dto.setTags(question.getTags().stream().map(TagDto::fromTag).collect(Collectors.toSet()));
            dto.setVoteCount(question.getViewCount());
            dto.setTime(question.getCreatedDate().getTime());
            dto.setVoteCount(question.getVoteCount());
            if (question.getCreatedBy() != null) {
                dto.setAuthor(UserDto.fromUser(question.getCreatedBy()));
            }
            if (this.vote != null) {
                dto.setVote(QuestionVoteDto.fromQuestionVoteDto(this.vote));
            }
            if (this.answers != null) {
                dto.setAnswers(this.answers.stream().map(AnswerDto::fromAnswer).collect(Collectors.toList()));
            }
            return dto;
        }
    }
}