package restapi.demo.restapi.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Setter
@Getter
public class QuestionListItemDto {

    private Long id;

    private String title;

    private String description;

    private Set<TagDto> tags;

    private Long viewCount;

    private Long voteCount;

    private Long time;

    private UserDto author;
}
