package restapi.demo.restapi.dto;

import lombok.Getter;
import lombok.Setter;
import restapi.demo.restapi.entity.QuestionVote;

import javax.validation.constraints.NotNull;

@Setter
@Getter
public class QuestionVoteDto {

    private Long id;

    @NotNull
    private Long questionId;

    @NotNull
    private VoteType type;

    public static QuestionVoteDto fromQuestionVoteDto(QuestionVote vote) {
        QuestionVoteDto dto = new QuestionVoteDto();
        dto.setId(vote.getId());
        dto.setQuestionId(vote.getQuestion().getId());
        dto.setType(vote.getType());
        return dto;
    }
}
