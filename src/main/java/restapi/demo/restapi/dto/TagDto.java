package restapi.demo.restapi.dto;

import lombok.Getter;
import lombok.Setter;
import restapi.demo.restapi.entity.Tag;

 import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Setter
@Getter
public class TagDto {

    private Long id;

    @NotNull(message = "Not Null")
    @Size(min = 3, message = "Min 3")
    private String title;

    public static TagDto fromTag(Tag tag) {
        TagDto dto = new TagDto();
        dto.setId(tag.getId());
        dto.setTitle(tag.getTitle());
        return dto;
    }
}
