package restapi.demo.restapi.dto;

public enum VoteType {
    UP, DOWN
}
