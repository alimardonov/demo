package restapi.demo.restapi.dto;

public enum UserRole {
    ADMINISTRATOR, MODERATOR, USER
}
