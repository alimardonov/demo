package restapi.demo.restapi.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Setter
@Getter
public class AnswerForm {

    @NotNull
    private Long questionId;

    @Size(min = 5)
    private String content;
}
