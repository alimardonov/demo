package restapi.demo.restapi.dto;

import lombok.Getter;
import lombok.Setter;
import org.bouncycastle.crypto.agreement.srp.SRP6Client;

@Setter
@Getter
public class ErrorDto {

    private int code;

    private String error;

    private String description;

    public static ErrorDto fromException(RuntimeException e) {
        ErrorDto dto = new ErrorDto();
        dto.setCode(1);
        dto.error = "ERROR";
        dto.description = e.getLocalizedMessage();
        return dto;
    }

    public static ErrorDto notFoundException(int id, String error, String description) {
        ErrorDto dto = new ErrorDto();
        dto.setCode(id);
        dto.setError(error);
        dto.setDescription(description);
        return dto;
    }
}
