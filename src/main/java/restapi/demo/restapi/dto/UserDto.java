package restapi.demo.restapi.dto;

import lombok.Getter;
import lombok.Setter;
import restapi.demo.restapi.entity.User;

@Setter
@Getter
public class UserDto {
    private Long id;

    private String fullName;

    public static UserDto fromUser(User user){
        UserDto dto = new UserDto();
        dto.setFullName(user.getFullName());
        dto.setId(user.getId());
        return dto;
    }
}
