package restapi.demo.restapi.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import restapi.demo.restapi.dto.AnswerForm;
import restapi.demo.restapi.service.AnswerService;

@RestController
@RequestMapping("api/v1/answer")
public class AnswerController {

    private final AnswerService answerService;

    public AnswerController(AnswerService answerService) {
        this.answerService = answerService;
    }

    @PostMapping
    public ResponseEntity add(@RequestBody AnswerForm form) {
        return ResponseEntity.ok(answerService.add(form));
    }

    @PutMapping("{id}")
    public ResponseEntity update(@RequestBody AnswerForm form, @PathVariable Long id) {
        return ResponseEntity.ok(answerService.update(id, form));
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        return ResponseEntity.ok(answerService.delete(id));
    }
}
