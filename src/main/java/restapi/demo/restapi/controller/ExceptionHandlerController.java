package restapi.demo.restapi.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import restapi.demo.restapi.dto.ErrorDto;
import restapi.demo.restapi.exceptions.CategoryNotFoundException;

@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler({RuntimeException.class})
    public ResponseEntity generalErrorHandler(RuntimeException e) {
        return ResponseEntity.badRequest()
                .body(ErrorDto.fromException(e));
    }

    @ExceptionHandler({CategoryNotFoundException.class})
    public ResponseEntity categoryNotFoundException(CategoryNotFoundException e) {
        return ResponseEntity.badRequest()
                .body(ErrorDto.notFoundException(5, "INVALID_CATEGORY_ID", e.getLocalizedMessage()));
    }
}
