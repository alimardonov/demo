package restapi.demo.restapi.controller;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import restapi.demo.restapi.dto.TagDto;
import restapi.demo.restapi.service.TagService;

import javax.validation.Valid;

@RestController
@RequestMapping("api/v1/tag")
public class TagController {
    private final TagService tagService;

    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping
    public ResponseEntity findAll(Pageable pageable) {
        return ResponseEntity.ok(tagService.findAll(pageable));
    }

    @GetMapping("search")
    public ResponseEntity search(@RequestParam String searchTerm, Pageable pageable) {
        return ResponseEntity.ok(tagService.search(searchTerm, pageable));
    }

    @PostMapping
    public ResponseEntity add(@Valid @RequestBody TagDto form) {
        return ResponseEntity.ok(tagService.add(form));
    }

    @PutMapping("{id}")
    public ResponseEntity update(@PathVariable Long id, @RequestBody TagDto dto) {
        return ResponseEntity.ok(tagService.update(id, dto));
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        return ResponseEntity.ok(tagService.delete(id));
    }
}
