package restapi.demo.restapi.controller;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import restapi.demo.restapi.dto.QuestionForm;
import restapi.demo.restapi.service.QuestionService;

@RestController
@RequestMapping("api/v1/question")
public class QuestionController {
    private final QuestionService questionService;

    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping("{id}")
    public ResponseEntity findOne(@PathVariable Long id) {
        return ResponseEntity.ok(questionService.findOne(id));
    }

    @GetMapping("{id}/list")
    public ResponseEntity list(@PathVariable(name = "id") Long categoryId, Pageable pageable) {
        return ResponseEntity.ok(questionService.findAllByCategory(categoryId, pageable));
    }

    @PostMapping()
    public ResponseEntity add(@RequestBody QuestionForm form) {
        return ResponseEntity.ok(questionService.add(form));
    }

    @PutMapping ("{id}")
    public ResponseEntity update(@PathVariable Long id, QuestionForm form) {
        return ResponseEntity.ok(questionService.update(id, form));
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        return ResponseEntity.ok(questionService.delete(id));
    }
}