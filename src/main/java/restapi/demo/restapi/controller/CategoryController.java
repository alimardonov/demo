package restapi.demo.restapi.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import restapi.demo.restapi.dto.CategoryDto;
import restapi.demo.restapi.service.CategoryService;

@RestController
@RequestMapping("api/v1/category")
public class CategoryController {

    private final CategoryService service;

    public CategoryController(CategoryService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity findByParent(@RequestParam(required = false) Long parentId) {
        return ResponseEntity.ok(service.findAll(parentId));
    }

    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable Long id) {
        return ResponseEntity.ok(service.findOne(id));
    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR', 'MODERATOR')")
    public ResponseEntity add(@RequestBody CategoryDto dto) {
        return ResponseEntity.ok(service.add(dto));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR', 'MODERATOR')")
    public ResponseEntity update(@PathVariable Long id, @RequestBody CategoryDto dto) {
        return ResponseEntity.ok(service.update(id, dto));
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    public ResponseEntity delete(@PathVariable Long id) {
        return ResponseEntity.ok(service.delete(id));
    }

}