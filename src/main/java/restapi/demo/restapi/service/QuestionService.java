package restapi.demo.restapi.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import restapi.demo.restapi.dto.QuestionDto;
import restapi.demo.restapi.dto.QuestionForm;
import restapi.demo.restapi.dto.QuestionListItemDto;

public interface QuestionService {

    Page<QuestionListItemDto> findAllByCategory(Long categoryId, Pageable pageable);

    QuestionDto findOne(Long id);

    QuestionListItemDto add(QuestionForm form);

    QuestionListItemDto update(Long id, QuestionForm form);

    QuestionListItemDto delete(Long id);
}
