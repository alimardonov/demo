package restapi.demo.restapi.service;

import org.springframework.lang.Nullable;
import restapi.demo.restapi.dto.CategoryDto;

import java.util.List;

public interface CategoryService {

    List<CategoryDto> findAll(@Nullable Long parentId);

    CategoryDto findOne(Long id);

    CategoryDto add(CategoryDto form);

    CategoryDto update(Long id, CategoryDto form);

    CategoryDto delete(Long id);
}
