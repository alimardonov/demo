package restapi.demo.restapi.service.impl;

import org.springframework.stereotype.Service;
import restapi.demo.restapi.dto.QuestionVoteDto;
import restapi.demo.restapi.service.AnswerVoteService;

@Service
public class AnswerVoteServiceImpl implements AnswerVoteService {
    @Override
    public QuestionVoteDto upVote(Long answerId, String username) {
        return null;
    }

    @Override
    public QuestionVoteDto downVote(Long answerId, String username) {
        return null;
    }

    @Override
    public QuestionVoteDto revertVote(Long answerId, String username) {
        return null;
    }
}
