package restapi.demo.restapi.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import restapi.demo.restapi.dto.TagDto;
import restapi.demo.restapi.entity.Tag;
import restapi.demo.restapi.repository.TagRepository;
import restapi.demo.restapi.service.TagService;

import java.util.Optional;

@Service
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public Page<TagDto> findAll(Pageable pageable) {
        return tagRepository.findAllNotDeleted(pageable)
                .map(TagDto::fromTag);
    }

    @Override
    public Page<TagDto> search(String searchTerm, Pageable pageable) {
        return tagRepository.findAllByTitleContainsAndDeletedFalse(searchTerm, pageable)
                .map(TagDto::fromTag);
    }

    @Override
    public TagDto add(TagDto dto) {
        Tag tag = new Tag();
        tag.setTitle(dto.getTitle());
        return TagDto.fromTag(tagRepository.save(tag));
    }

    @Override
    public TagDto update(Long id, TagDto form) {
        Optional<Tag> tagOptional = tagRepository.findById(id);
        tagOptional.ifPresent(tag -> tag.setTitle(form.getTitle()));
        return TagDto.fromTag(tagOptional.get());
    }

    @Override
    public TagDto delete(Long id) {
        return TagDto.fromTag(tagRepository.trash(id));
    }
}
