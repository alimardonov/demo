package restapi.demo.restapi.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import restapi.demo.restapi.dto.QuestionDto;
import restapi.demo.restapi.dto.QuestionForm;
import restapi.demo.restapi.dto.QuestionListItemDto;
import restapi.demo.restapi.entity.Answer;
import restapi.demo.restapi.entity.Category;
import restapi.demo.restapi.entity.Question;
import restapi.demo.restapi.entity.Tag;
import restapi.demo.restapi.repository.AnswerRepository;
import restapi.demo.restapi.repository.QuestionRepository;
import restapi.demo.restapi.service.QuestionService;
import restapi.demo.restapi.service.TagService;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;
    private final AnswerRepository answerRepository;
    private final EntityManager entityManager;
    private final TagService tagService;

    public QuestionServiceImpl(QuestionRepository questionRepository, AnswerRepository answerRepository, EntityManager entityManager, TagService tagService) {
        this.questionRepository = questionRepository;
        this.answerRepository = answerRepository;
        this.entityManager = entityManager;
        this.tagService = tagService;
    }


    @Override
    public Page<QuestionListItemDto> findAllByCategory(Long categoryId, Pageable pageable) {
        return questionRepository.findAllByCategory_IdAndDeletedFalse(categoryId, pageable)
                .map(question -> new QuestionDto.Builder(question).builder());
    }

    @Override
    public QuestionDto findOne(Long id) {
        Question question = questionRepository.getOne(id);
        questionRepository.save(question.incrementViewCount());
        List<Answer> answers = answerRepository.findAllByQuestionAndDeletedFalse(question);
        return new QuestionDto.Builder(question).answer(answers).builder();
    }

    @Override
    public QuestionListItemDto add(QuestionForm form) {
        return save(form, new Question());
    }

    @Override
    public QuestionListItemDto update(Long id, QuestionForm form) {
        return save(form, questionRepository.findById(id).get());
    }

    @Override
    public QuestionListItemDto delete(Long id) {
        return new QuestionDto.Builder(questionRepository.trash(id)).builder();
    }

    private QuestionListItemDto save(QuestionForm form, Question question) {
        question.setTitle(form.getTitle());
        question.setDescription(form.getDescription());
        question.setCategory(entityManager.getReference(Category.class, form.getCategoryId()));
        if (form.getTags() != null) {
            question.setTags(form.getTags().stream().map(dto -> {
                if (dto.getId() == null) {
                    dto = tagService.add(dto);
                }
                return entityManager.getReference(Tag.class, dto.getId());
            }).collect(Collectors.toSet()));
        }
        return new QuestionDto.Builder(question).builder();
    }

}
