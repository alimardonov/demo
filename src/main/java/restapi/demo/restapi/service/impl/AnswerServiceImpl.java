package restapi.demo.restapi.service.impl;

import org.springframework.stereotype.Service;
import restapi.demo.restapi.dto.AnswerDto;
import restapi.demo.restapi.dto.AnswerForm;
import restapi.demo.restapi.entity.Answer;
import restapi.demo.restapi.entity.Question;
import restapi.demo.restapi.repository.AnswerRepository;
import restapi.demo.restapi.service.AnswerService;

import javax.persistence.EntityManager;

@Service
public class AnswerServiceImpl implements AnswerService {

    private final AnswerRepository answerRepository;
    private final EntityManager entityManager;

    public AnswerServiceImpl(AnswerRepository answerRepository, EntityManager entityManager) {
        this.answerRepository = answerRepository;
        this.entityManager = entityManager;
    }

    @Override
    public AnswerDto add(AnswerForm form) {
        return save(new Answer(), form);
    }

    @Override
    public AnswerDto update(Long answerId, AnswerForm form) {
        return save(answerRepository.findById(answerId).get(), form);
    }

    @Override
    public AnswerDto delete(Long answerId) {
        return AnswerDto.fromAnswer(answerRepository.trash(answerId));
    }

    private AnswerDto save(Answer answer, AnswerForm form) {
        answer.setQuestion(entityManager.getReference(Question.class, form.getQuestionId()));
        answer.setContent(form.getContent());
        return AnswerDto.fromAnswer(answerRepository.save(answer));
    }
}