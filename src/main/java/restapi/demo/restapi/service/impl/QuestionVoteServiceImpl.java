package restapi.demo.restapi.service.impl;

import org.springframework.stereotype.Service;
import restapi.demo.restapi.dto.QuestionVoteDto;
import restapi.demo.restapi.dto.VoteType;
import restapi.demo.restapi.entity.Question;
import restapi.demo.restapi.entity.QuestionVote;
import restapi.demo.restapi.entity.User;
import restapi.demo.restapi.repository.QuestionRepository;
import restapi.demo.restapi.repository.QuestionVoteRepository;
import restapi.demo.restapi.repository.UserRepository;
import restapi.demo.restapi.service.QuestionVoteService;

import javax.persistence.EntityManager;
import java.util.Optional;

@Service
public class QuestionVoteServiceImpl implements QuestionVoteService {

    private final QuestionVoteRepository questionVoteRepository;
    private final QuestionRepository questionRepository;
    private final UserRepository userRepository;
    private final EntityManager entityManager;

    public QuestionVoteServiceImpl(QuestionVoteRepository questionVoteRepository, QuestionRepository questionRepository, UserRepository userRepository, EntityManager entityManager) {
        this.questionVoteRepository = questionVoteRepository;
        this.questionRepository = questionRepository;
        this.userRepository = userRepository;
        this.entityManager = entityManager;
    }

    @Override
    public QuestionVoteDto upVote(Long questionId, String username) {
        return vote(questionId, VoteType.UP, username);
    }

    @Override
    public QuestionVoteDto downVote(Long questionId, String username) {
        return vote(questionId, VoteType.DOWN, username);
    }

    @Override
    public QuestionVoteDto revertVote(Long questionId, String username) {
        Optional<User> optionalUser = userRepository.findByEmail(username);
        if (optionalUser.isPresent()) {
            Optional<QuestionVote> optionalQuestionVote = questionVoteRepository
                    .findByQuestion_IdAndCreatedByAndDeletedFalse(questionId, optionalUser.get());
            if (optionalQuestionVote.isPresent()) {
                QuestionVoteDto dto = QuestionVoteDto
                        .fromQuestionVoteDto(questionVoteRepository.trash(optionalQuestionVote.get().getId()));
                reCalculateQuestionVote(dto.getId());
                return dto;
            }
        }
        return null;
    }

    private void reCalculateQuestionVote(Long questionId) {
        Question question = questionRepository.findById(questionId).get();
        long count = questionVoteRepository.countByQuestionAndTypeAndDeletedFalse(question, VoteType.UP) -
                questionVoteRepository.countByQuestionAndTypeAndDeletedFalse(question, VoteType.DOWN);
        question.setVoteCount(count);
        questionRepository.save(question);
    }

    private QuestionVoteDto vote(Long questionId, VoteType type, String username){
        Optional<User> optionalUser = userRepository.findByEmail(username);
        if (optionalUser.isPresent()) {
            Optional<QuestionVote> optionalQuestionVote = questionVoteRepository
                    .findByQuestion_IdAndCreatedByAndDeletedFalse(questionId, optionalUser.get());
            if (!optionalQuestionVote.isPresent()) {
                QuestionVote vote = new QuestionVote();
                vote.setQuestion(entityManager.getReference(Question.class, questionId));
                vote.setType(type);
                QuestionVoteDto dto = QuestionVoteDto.fromQuestionVoteDto(questionVoteRepository.save(vote));
                reCalculateQuestionVote(dto.getQuestionId());
                return dto;
            }
        }
        return null;
    }
}
