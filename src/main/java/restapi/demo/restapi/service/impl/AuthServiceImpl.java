package restapi.demo.restapi.service.impl;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import restapi.demo.restapi.repository.UserRepository;

@Service
public class AuthServiceImpl {

    private static final String USERNAME = "legandry01@gmail.com";
    private static final String PASSWORD = "root123";

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public AuthServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

//    public Result signUp(User user, HttpServletRequest request) {

//        if (userRepository.existsByEmail(user.getEmail())) {
//            return new Result("Email address is already in use", true);
//        }
//        try {
//            user.setEmail(user.getEmail());
//            user.setPassword(passwordEncoder.encode(user.getPassword()));
//            user.setFullName(user.getFullName());
//            org.apache.catalina.User user1 = new User();
//        }catch (Exception e) {
//            System.out.printf("sdf");
//        }
//
//        return null;
//    }
}
