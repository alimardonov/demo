package restapi.demo.restapi.service.impl;

import org.springframework.stereotype.Service;
import restapi.demo.restapi.dto.CategoryDto;
import restapi.demo.restapi.entity.Category;
import restapi.demo.restapi.exceptions.CategoryNotFoundException;
import restapi.demo.restapi.repository.CategoryRepository;
import restapi.demo.restapi.service.CategoryService;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static restapi.demo.restapi.dto.CategoryDto.toDto;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository repository;
    private final EntityManager entityManager;

    public CategoryServiceImpl(CategoryRepository repository, EntityManager entityManager) {
        this.repository = repository;
        this.entityManager = entityManager;
    }

    @Override
    public List<CategoryDto> findAll(Long parentId) {
        return repository.findAllByParent_IdAndDeletedFalse(parentId)
                .stream()
                .map(CategoryDto::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public CategoryDto findOne(Long id) {
        Optional<Category> optionalCategory = repository.findById(id);
        if (!optionalCategory.isPresent() || optionalCategory.get().isDeleted()) {
            throw new CategoryNotFoundException(id);
        }
        return toDto(optionalCategory.get());
    }

    @Override
    public CategoryDto add(CategoryDto form) {
        return toDto(repository.save(fillCategory(new Category(), form)));
    }

    @Override
    public CategoryDto update(Long id, CategoryDto form) {
        return toDto(repository.save(fillCategory(repository.getOne(id), form)));
    }

    @Override
    public CategoryDto delete(Long id) {
        return toDto(repository.trash(id));
    }

    private Category fillCategory(Category category, CategoryDto dto) {

        category.setTitle(dto.getTitle());
        category.setDescription(dto.getDescription());
        category.setIconCode(dto.getIcon());

        if (dto.getParentId() != null){
            category.setParent(entityManager.getReference(Category.class, dto.getParentId()));
        }
        return category;
    }
}
