package restapi.demo.restapi.service;

import restapi.demo.restapi.dto.AnswerDto;
import restapi.demo.restapi.dto.AnswerForm;

public interface AnswerService {

    AnswerDto add(AnswerForm form);

    AnswerDto update(Long answerId, AnswerForm form);

    AnswerDto delete(Long answerId);
}
