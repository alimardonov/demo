package restapi.demo.restapi.service;

import restapi.demo.restapi.dto.QuestionVoteDto;

public interface QuestionVoteService {

    QuestionVoteDto upVote(Long questionId, String username);

    QuestionVoteDto downVote(Long questionId, String username);

    QuestionVoteDto revertVote(Long questionId, String username);
}
