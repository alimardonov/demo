package restapi.demo.restapi.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import restapi.demo.restapi.dto.TagDto;

public interface TagService {

    Page<TagDto> findAll(Pageable pageable);

    Page<TagDto> search(String searchTerm, Pageable pageable);

    TagDto add(TagDto dto);

    TagDto update(Long id, TagDto form);

    TagDto delete(Long id);
}
