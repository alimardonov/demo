package restapi.demo.restapi.service;

import restapi.demo.restapi.dto.QuestionVoteDto;

public interface AnswerVoteService {
    // TODO: 10.03.2021 must have fill this and iml, controller.

    QuestionVoteDto upVote(Long answerId, String username);

    QuestionVoteDto downVote(Long answerId, String username);

    QuestionVoteDto revertVote(Long answerId, String username);
}